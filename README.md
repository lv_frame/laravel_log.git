# Laravel Log 使用教程

开发目的：监控Laravel系统中的请求信息，报错、超时等情况。

## 1. 使用说明

### 1.1 下载依赖

```shell
composer require hzanchu/laravel_log
```

### 1.2 注册中间件

`app/Http/Kernel.php`:

```php
protected $middlewareGroups = [
    'api' => [
        Anchu\LaravelLog\Middlewares\LogRequest::class,
    ],
];
```

### 1.3 注入代码

捕获错误信息：`app/Exceptions/Handler.php`

```php
public function register()
{
    $this->reportable(function (Throwable $e) {
        // 捕获异常到临时变量
        $_COOKIE['exception'] = json_encode([
            'code' => $exception->getCode(),
            'file' => $exception->getFile(),
            'line' => $exception->getLine(),
            'message' => $exception->getMessage()
        ]);
    });
}
```

### 1.4 配置参数
在多个数据库的情况下，需要如下参数指定链接：
`.env`：
```shell
MONITOR_DB_CONNECTION=mysql
LOG_TOKEN=xxxx
```



