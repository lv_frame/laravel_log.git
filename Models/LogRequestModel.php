<?php

namespace Anchu\LaravelLog\Models;

use Anchu\Restful\Models\Columns\ColumnFactory;
use Anchu\Restful\Models\Model;
use Anchu\Restful\Models\TableSchema;

class LogRequestModel extends Model
{
    protected $guarded = [];

    protected $table = 'log_request';

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->connection = env('MONITOR_DB_CONNECTION', 'mysql');
    }

    /*
     * 表设计
     */
    public static function table(): TableSchema
    {
        // TODO: Implement table() method.
        $column = new ColumnFactory();
        return new TableSchema(
            tableName: 'log_request',
            comment: '日志系统-请求日志',
            columns: [
                'client_ip' => $column->string(label: '客户端ip', length: 100),
                'host' => $column->string(label: 'host和端口号', length: 100),
                'uri' => $column->string(label: '地址', length: 1000),
                'method' => $column->string(label: '请求方法', length: 10),
                'query' => $column->string(label: '参数', length: 500),
                'body' => $column->string(label: '请求体', length: 500),
                'response' => $column->text(label: '返回内容'),
                'duration' => $column->decimal(label: '响应时长', precision: 10, scale: 8),
                'server' => $column->text(label: '服务器信息'),
                'exception' => $column->string(label: '报错信息'),
                'track' => $column->text(label: '详细报错信息'),
            ],
        );
    }

    // 获取请求信息
    public static function create($response = null)
    {
        try {
            $server = $_SERVER;
            foreach ($server as $key => $value) {
                $check = false;
                $targets = ['password', 'secret'];
                foreach ($targets as $target) {
                    if (stripos($key, $target) !== false) {
                        $check = true;
                        break;
                    }
                }
                if ($check) {
                    $server[$key] = '******';
                }
            }
            $data = [
                'client_ip' => request()->ip(),
                'host' => substr(request()->url(), 0, strpos(request()->url(), request()->path())),
                'uri' => request()->path(),
                'method' => request()->getMethod(),
                'query' => json_encode(request()->query()),
                'body' => json_encode(request()->all()),
                'response' => isset($_COOKIE['exception']) ? '' : (is_null($response) ? '' : $response->getContent()),
                'duration' => microtime(true) - LARAVEL_START,
                'server' => json_encode($server),
                'exception' => $_COOKIE['exception'] ?? '',
                'track' => '',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $id = self::query()->insertGetId($data);

            // 清理一个月前的日志
            self::clear($id);
        } catch (\Exception $e) {
            self::log('$e:' . $e->getMessage());
        }

    }

    public static function log($message)
    {
        file_put_contents('/tmp/log', $message . PHP_EOL, 8);
    }

    public static function clear($id = 0)
    {
        $max = env('LARAVEL_LOG_CLEAR_MAX', 5000);
        $min = env('LARAVEL_LOG_CLEAR_MAX', 2000);
        if ($id % $max == 0) {
            self::query()->where('id', '<=', $id - $min)->delete();
        }
    }
}
